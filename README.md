# README #


### How do I get set up? ###

Download and run MongoDB (tested with version 3.4). 
Database should be running on port 27017 (default). Change this in application.yml if you want.


### Endpoints ###
Customers endpoint: localhost:8080/api/customers

To list all customers:
GET: localhost:8080/api/customers

To list a specific customer:
GET: localhost:8080/api/customers/some_id

To save a customer
POST localhost:8080/api/customers


You need to be authenticated to do any calls. Once the application is started, it will generate a token. Copy and paste this token in your Auhorization Header, eg:

Bearer eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJTbWFydEZhY3R1dXIgQXBwIiwiaWF0IjoxNTEwNDY5NTAzLCJzdWIiOiJub25hbWVAZ21haWwuY29tIiwiZXhwIjoxNTExMDc0MzAzLCJmaXJzdE5hbWUiOiJKb2huIiwibGFzdE5hbWUiOiJEb2UiLCJlbWFpbCI6Im5vbmFtZUBnbWFpbC5jb20iLCJyb2xlcyI6WyJhZG1pbiJdfQ.MoVo3bH6kH6XVtmAckfAeFDsMKYK4gjP7NFiszXHPrLbJwu6ce2H-Sssw1M_PE9TYmdaxS4_6tQdcH0l0dhL5Q


