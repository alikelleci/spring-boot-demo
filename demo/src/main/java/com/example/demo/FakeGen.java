package com.example.demo;


import com.example.demo.model.Customer;
import com.example.demo.model.User;
import com.example.demo.security.JwtService;
import com.github.javafaker.Faker;
import com.mongodb.MongoClient;
import lombok.extern.log4j.Log4j;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Log4j
@Profile("dev")
@Component
public class FakeGen implements ApplicationRunner {

    @Autowired
    JwtService jwtService;

    @Autowired
    private MongoClient mongoClient;

    @Autowired
    private Datastore datastore;


    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Importing test data...");

        // Clear database
        mongoClient.dropDatabase("test");

        // Faker instancedatastore
        Faker faker = new Faker();


        // Users
        datastore.save(User.builder()
                .email("admin@test.com")
                .password("654321")
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .role("admin")
                .build());


        // Customers
        List<Customer> customers = new ArrayList();
        for (int i = 0; i < 20; i++) {
            customers.add(Customer.builder()
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .company(faker.company().name())
                    .email(faker.internet().emailAddress())
                    .phone(faker.phoneNumber().phoneNumber())
                    .build());
        }
        datastore.save(customers);


        //Generate token
        generateToken();
    }

    private void generateToken() {
        log.info("Generating token...");

        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .email("noname@gmail.com")
                .role("admin")
                .build();

        String token = jwtService.generateToken(user);
        log.info("*************************************************************");
        log.info(token);
        log.info("*************************************************************");
    }


}