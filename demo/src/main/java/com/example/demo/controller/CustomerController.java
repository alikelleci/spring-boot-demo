package com.example.demo.controller;


import com.example.demo.controller.expetions.ResourceNotFoundException;
import com.example.demo.dto.CustomerDto;
import com.example.demo.model.Customer;
import com.example.demo.service.CustomerService;
import com.mongodb.WriteResult;
import ma.glasnost.orika.MapperFacade;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/api/customers")
public class CustomerController {

    @Autowired
    private CustomerService service;


    @Autowired
    private MapperFacade mapper;

    @RequestMapping(method = RequestMethod.GET)
    public List<CustomerDto> getAll() {
        return service.getAll().stream()
                .map(customer -> mapper.map(customer, CustomerDto.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CustomerDto get(@PathVariable ObjectId id) {
        Customer customer = service.get(id);
        if (customer != null) {
            return mapper.map(customer, CustomerDto.class);
        }
        throw new ResourceNotFoundException("Resource not found.");
    }

    @RequestMapping(method = RequestMethod.POST)
    public Key<Customer> post(@Valid @RequestBody CustomerDto dto) {
        Customer customer = mapper.map(dto, Customer.class);
        return service.save(customer);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public WriteResult delete(@PathVariable ObjectId id) {
        return service.delete(id);
    }


}
