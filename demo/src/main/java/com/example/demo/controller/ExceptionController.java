package com.example.demo.controller;

import com.example.demo.controller.misc.Error;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Log4j
@RestControllerAdvice
public class ExceptionController {




    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST) // 400
    public Error exception(HttpServletRequest request, MethodArgumentNotValidException e) {
        // Log error
        log.error(e.getMessage());

        List<Error> errors = new ArrayList<>();

        BindingResult result = e.getBindingResult();
        result.getFieldErrors().forEach(error -> errors.add(Error.builder()
                .title(error.getDefaultMessage())
                .build()));

        // Send response
        return Error.builder()
                .code(0)
                .title("Validation error.")
                .detail(e.getMessage())
                .path(request.getRequestURI())
                .property("errors", errors)
                .build();
    }


}
