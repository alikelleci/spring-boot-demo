package com.example.demo.controller.misc;

import lombok.*;

import java.util.Map;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error {

    private int code;
    private String title;
    private String detail;
    private String path;
    @Singular
    private Map<String, Object> properties;
}
