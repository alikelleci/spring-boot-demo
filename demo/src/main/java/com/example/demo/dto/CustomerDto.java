/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.dto;


import lombok.*;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.List;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {

    private ObjectId id;

    @NotBlank(message = "Firstname may not be empty")
    @Length(max = 50, message = "Firstname is too long")
    private String firstName;

    @NotBlank(message = "Lastname may not be empty")
    @Length(max = 50, message = "Lastname is too long")
    private String lastName;

    private String company;

    @NotBlank(message = "Email may not be empty")
    @Length(max = 50, message = "Email is too long")
    @Email(message = "Email is not valid")
    private String email;

    private String phone;
}
