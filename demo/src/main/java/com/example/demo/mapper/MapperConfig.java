package com.example.demo.mapper;


import com.example.demo.dto.CustomerDto;
import com.example.demo.mapper.mappers.CustomerMapper;
import com.example.demo.model.Customer;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {


    @Autowired
    private CustomerMapper customerMapper;


    @Bean
    public MapperFacade mapper() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(Customer.class, CustomerDto.class).customize(customerMapper).byDefault().register();
        return mapperFactory.getMapperFacade();
    }
}
