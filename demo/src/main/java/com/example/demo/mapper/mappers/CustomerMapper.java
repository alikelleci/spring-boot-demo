package com.example.demo.mapper.mappers;



import com.example.demo.dto.CustomerDto;
import com.example.demo.model.Customer;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper extends CustomMapper<Customer, CustomerDto> {


    @Override
    public void mapAtoB(Customer customer, CustomerDto dto, MappingContext context) {

    }

    @Override
    public void mapBtoA(CustomerDto dto, Customer customer, MappingContext context) {

    }


}
