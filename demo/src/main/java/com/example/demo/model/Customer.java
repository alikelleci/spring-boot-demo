/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;


import lombok.*;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.List;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity("customers")
public class Customer  {

    @Id
    private ObjectId id;
    @Indexed
    private String firstName;
    @Indexed
    private String lastName;
    private String company;
    private String email;
    private String phone;
}
