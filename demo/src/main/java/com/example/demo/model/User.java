package com.example.demo.model;


import lombok.*;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@Entity("users")
public class User {

    @Id
    private ObjectId id;
    @NotNull(message = "Name may not be null")
    @Indexed
    private String firstName;
    @NotNull(message = "Surname may not be null")
    @Indexed
    private String lastName;
    @NotNull(message = "Email may not be null")
    @Indexed(options = @IndexOptions(unique = true))
    private String email;
    @NotNull(message = "Password may not be null")
    private String password;
    @Singular
    private List<String> roles;
    @Indexed
    private boolean blocked;
    @Indexed
    private boolean expired;

}
