package com.example.demo.repository;

import com.example.demo.model.Customer;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepository extends BasicDAO<Customer, ObjectId> {


    @Autowired
    protected CustomerRepository(Datastore ds) {
        super(ds);
    }

    public List<Customer> getAll() {
        return this.createQuery()
                .disableValidation()
                .order("firstName, lastName")
                .asList();
    }

}
