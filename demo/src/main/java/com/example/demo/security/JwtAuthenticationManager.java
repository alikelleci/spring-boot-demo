package com.example.demo.security;


import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class JwtAuthenticationManager implements AuthenticationManager {

    @Autowired
    private JwtService jwtService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = ((TokenAuthentication) authentication).getToken();
        if (token != null) {
            Claims claims = jwtService.parseToken(token);
            if (claims != null) {
                UserDetails userDetails = jwtService.extractUserDetails(claims);
                List<GrantedAuthority> authorityList = toAuthorities(userDetails.getRoles());
                return new TokenAuthentication(token, userDetails, authorityList);
            }
        }
        return authentication;
    }


    private List<GrantedAuthority> toAuthorities(List<String> roles) {
        if (roles != null) {
            return roles.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }


}
