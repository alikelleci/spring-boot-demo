package com.example.demo.security;

import com.example.demo.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.log4j.Log4j;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Log4j
@Service
public class JwtService {

    private final String SECRET = "secret";

    public String generateToken(User user) {
        Claims claims = Jwts.claims()
                .setIssuer("SmartFactuur App")
                .setIssuedAt(new Date())
                .setSubject(user.getEmail())
                .setExpiration(DateTime.now().plusDays(7).toDate());

        if (user.getId() != null) {
            claims.put("id", user.getId().toString());
        }
        claims.put("firstName", user.getFirstName());
        claims.put("lastName", user.getLastName());
        claims.put("email", user.getEmail());
        claims.put("roles", user.getRoles());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public Claims parseToken(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            log.error("JWT token is invalid: " + e.getMessage());
            return null;
        }
    }


    public UserDetails extractUserDetails(Claims claims) {
        return UserDetails.builder()
                .id(claims.get("id", String.class))
                .firstName(claims.get("firstName", String.class))
                .lastName(claims.get("lastName", String.class))
                .email(claims.get("email", String.class))
                .roles(claims.get("roles", List.class))
                .build();
    }
}
