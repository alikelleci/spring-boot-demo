package com.example.demo.security;


import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;


public class TokenAuthentication extends AbstractAuthenticationToken {

    private final String token;

    public TokenAuthentication(String token) {
        super(new ArrayList<SimpleGrantedAuthority>());
        super.setAuthenticated(false);
        super.setDetails(null);
        this.token = token;

    }

    public TokenAuthentication(String token, UserDetails userDetails, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        super.setAuthenticated(true);
        super.setDetails(userDetails);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        if (this.getDetails() != null) {
            return ((UserDetails) this.getDetails()).getEmail();
        }
        return null;
    }
}