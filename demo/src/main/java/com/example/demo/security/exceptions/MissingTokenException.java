package com.example.demo.security.exceptions;

import org.springframework.security.core.AuthenticationException;


public class MissingTokenException extends AuthenticationException {

    public MissingTokenException(String msg) {
        super(msg);
    }
}
