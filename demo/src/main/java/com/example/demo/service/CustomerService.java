package com.example.demo.service;

import com.example.demo.model.Customer;
import com.example.demo.repository.CustomerRepository;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<Customer> getAll() {
        return repository.getAll();
    }


    public Customer get(ObjectId id) {
        return repository.get(id);
    }


    public Key<Customer> save(Customer customer) {
        return repository.save(customer);
    }

    public WriteResult delete(ObjectId id) {
        return repository.deleteById(id);
    }

}
